// store.js
import { defineStore } from 'pinia'
import {computed, ref} from 'vue'
export const useCounterStore = defineStore('counter', () => {
    const count = ref(0)

    const humans = ref([
        {id: 1, name:'nam turong', age: 22},
        {id: 2, name: 'thai duong', age: 23},
        {id: 3, name: 'khanh tung', age: 22},
    ])

    function increment() {
        count.value++
    }

    function addUser(human) {
        humans.value.push({
            id: humans.value.length + 1,
            ...human
        })
    }

    return {
        count,
        increment,
        humans,
        addUser,
    }
})
