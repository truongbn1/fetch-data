import { createI18n } from "vue-i18n";

const messages = {
  en: {
    message: {
      hello: "Hello world!",
    },
  },
  fr: {
    message: {
      hello: "Bonjour le monde!",
    },
  },
};

const i18n = createI18n({
  locale: "fr", // set default locale
  messages, // set locale messages
});

export default i18n;
