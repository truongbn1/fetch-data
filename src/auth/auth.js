const auth = {
    isLoggedIn() {
      // Check if user is logged in
      const user = localStorage.getItem('user')
      return user !== null && user !== undefined
    }
  }
  
export default auth
  