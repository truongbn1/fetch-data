import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import "./assets/main.css";
import i18n from "../src/translate/i18n";
import { createPinia } from "pinia";

const app = createApp(App);
const pinia = createPinia();
app.use(router);
app.use(i18n);
app.directive("focus", {
  mounted(el) {
    el.focus();
  },
});
app.use(pinia)
app.mount("#app");
