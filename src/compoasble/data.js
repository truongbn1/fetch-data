import axios from "axios";
import { ref } from "vue";
export function postFetch(url) {
  const data = ref("");

  async function getPost() {
    try {
      const response = await axios.get(url);
      data.value = await response.data;
      // console.log(users.value);
    } catch (error) {
      console.error(error);
    }
  }

  getPost();

  return {
    data,
  };
}
