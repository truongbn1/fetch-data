import { createRouter, createWebHistory } from "vue-router";
import Show from "../views/Show.vue";
import auth from "@/auth/auth";

const routes = [
  {
    path: "/login",
    name: "Login",
    component: () => import("../views/Login.vue"),
  },
  {
    path: "/",
    name: "Home",
    component: () => import("@/views/Home.vue"),
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/about/:id",
    name: "About",
    component: () => import("@/views/About.vue"),
    children: [
      {
        path: "show",
        component: Show,
        props: true,
        meta: {
          requiresAuth: true,
        },
      },
    ],
  },
  {
    path: '/data',
    name: 'Data',
    component: () => import('@/views/DataFetching.vue'),
    children: [ {
      path: ':id',
      name: 'data.show',
      component: () => import('@/views/Data.vue'),
    }
    ]
  }
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

router.beforeEach((to, from) => {
  // instead of having to check every route record with
  // to.matched.some(record => record.meta.requiresAuth)
  if (to.meta.requiresAuth && !window.user) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    return {
      name: "Login",
      // save the location we were at to come back later
    };
  }
});

export default router;
